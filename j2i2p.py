import codecs
import datetime
import json
import os
import re
import shutil
import sys

from zeep import Client


def zfill_num(num):
    """
    무작위로 작명된 xml 파일 이름의 형식을 통일시켜 주기 위한 함수.
    :return: 앞자리에 0이 붙는 4자리의 숫자.
    """
    return str(num).zfill(5)


def makefilename(check, number):
    """
    스토리, 스프레드의 파밀명을 설정해서 리턴.
    :param check: 1- 스프레스, 0- 스토리
    :param number: 설정하고자 하는 파일명
    :return: string
    """
    if check:
        return "Spread_u" + zfill_num(number)
    else:
        return "Story_u" + zfill_num(number)


class CreatePdf:

    def set_idserver_url(self,url):
        self.indesign_url= url

    def set_user_name(self,name):
        self.output = name

    def set_user_json(self, json_path):
        with open(json_path) as json_file:
            json_data = json.load(json_file)
        # self.json_list = json.loads(json_data)
        self.json_list = json_data[self.result]
        # self.total_count = len(self.json_list) # json 전체 출력.  
        self.total_count = 3 # json 3챕터만 출력, 테스트용.

    def set_file_dir(self,path):
        self.os_dir = path
        self.template_dir = self.os_dir + self.output + 'template/'
        self.tmp_dir = self.os_dir + self.output + "tmp/"
        self.idml = os.path.abspath(self.os_dir + 'example/mytemplates22.idml')
        self.spread_dir = self.tmp_dir + "Spreads/"
        self.story_dir = self.tmp_dir + "Stories/"
        # self.output_dir = os.path.abspath('/data/')
        self.output_dir = os.path.abspath(self.os_dir + "data")
        # self.result_dir = os.path.abspath('data/')
        self.result_dir = os.path.abspath(self.os_dir + 'data/')

    def __init__(self, *args, **kwargs) -> None:

        # 바꿔넣을 json list, change_list의 value 를 key로 검색.
        # self.json_list = kwargs['lists']
        # musubiya 전용.

        self.checkmean = 0
        self.total_count = "count"
        self.result = "results"
        self.count = "count"
        self.text_list = "notevocas"
        # self.text_per_page = kwargs['text_per_page']
        self.indesign_url = "http://localhost"
        # self.output = kwargs['username']
        self.output = ""
        self.templates_info = {"cover": 0, "main": 3, "main2": 4, "test": 2, "chap": 1}
        self.word_per_page = 10

        # 기존 스프레드.
        self.spreads = []
        # 새로운 리스트.
        self.new_spread_templates = []

        # 기존 - 변경 스프레드명 저장 리스트.
        self.story_lists_templates = {}
        self.list_new_story ={}

    def create(self):
        """
        json 으로 들어온 정보를 idml과 pdf 로 내보내는 함수.
        :return: Http status code
        """
        self.unpack_idml()
        self.init_spread()
        self.init_story()
        self.create_page()
        self.create_idml()
        print("please wait..")
        self.export(self.output, self.output_dir)

        try:
            shutil.move(self.output_dir+self.output+".idml", self.result_dir)
            # os.remove(self.result_dir + "/" + self.output + ".idml")
        except:
            # 결과저장할 파일이 이미 존재하는경우
            # os.remove(self.result_dir + "/" + self.output + ".idml")
            # shutil.move(self.output_dir +"/"+ self.output + ".idml", self.result_dir)
            # os.remove(self.result_dir + "/" + self.output + ".idml")
            pass
        #
        # try:
        #     shutil.move(self.output_dir +self.output+".pdf", self.result_dir)
        #     # os.remove(self.result_dir + "/" + self.output + ".pdf")
        # except:
        #     # 결과저장할 파일이 이미 존재하는경우
        #     # os.remove(self.result_dir + self.output + ".pdf")
        #     # shutil.move(self.output_dir + self.output + ".pdf", self.result_dir)
        #     # os.remove(self.result_dir + self.output + ".pdf")
        #     pass
        # shutil.rmtree("../"+self.output)
        print(str(datetime.datetime.today()) + ".Pdf created")
        return {
            'idml': self.output +".idml",
            'pdf': self.output +".pdf"
        }

    def unpack_idml(self):
        """
        xml 들의 묶음으로 되어있는 idml 파일을 묶음해제하여 사용자가 편집가능하게 함.
        """
        try:
            shutil.unpack_archive(self.idml, self.template_dir, 'zip')
            shutil.copytree(self.template_dir, self.tmp_dir)
        except FileExistsError:
            shutil.rmtree(self.tmp_dir)
            shutil.copytree(self.template_dir, self.tmp_dir)

    # 전부 종료된 후 idml 로 변환.
    def create_idml(self):
        for old in self.new_spread_templates:
            try:
                os.remove(self.spread_dir + old + ".xml")
            except:
                pass
        for old in self.list_new_story.keys():
            os.remove(self.story_dir + "Story_" +old+".xml")
        shutil.make_archive(self.output_dir+"/"+self.output + ".idml", 'zip', self.tmp_dir)
        shutil.move(self.output_dir + "/"+self.output + ".idml" + '.zip', self.output_dir + '/' + self.output+".idml")

        shutil.rmtree(self.tmp_dir)
        shutil.rmtree(self.template_dir)
        print(self.output_dir + self.output + ".idml 생성 완료.")

    # idml 을 pdf 로 변환.
    def export(self, name, dst):
        """

        idml 파일을 pdf 로 내보내기.
        indesign cc server 를 통해야만 가능하다. 파이썬을 지원하지 않기 때문에 js 를 통하여 변환.
        indesign cc server 폴더로 이동 -> indesignserver -port 12345 사용.

        """

        # 스크립트 경로
        exportScriptPath = self.os_dir + "/export_pdf.jsx"
        
        # indesign cc server
        client = Client(self.indesign_url + ':12345?wsdl')

        # Create SOAP call params object, based on IDS wsdl
        # For details go to http://localhost:12345?wsdl
        RunScriptParameters = client.get_type('ns0:RunScriptParameters')
        IDSP_ScriptArg = client.get_type('ns0:IDSP-ScriptArg')

        args = {
            "name": name,
            # "time": time,
            "dst": dst
        }
        self.params = RunScriptParameters("ah", "javascript", exportScriptPath,
                                     [IDSP_ScriptArg(key, value) for key, value in args.items()])
        try:
            self.result = client.service.RunScript(self.params)
        except:
            self.params = RunScriptParameters("ah", "javascript", os.path.abspath('export_pdf.jsx'),
                                         [IDSP_ScriptArg(key, value) for key, value in args.items()])
            self.result = client.service.RunScript(self.params)
        # print(self.result)

    def init_spread(self):
        """
        스프레드 템플릿 규칙성 적용 및 정리.
        1. 초기 스프레드는 아이디와 스프레드 순서 모두 알수없는 규칙으로 생성됨.
        따라서 사용자가 식별, 접근 가능한 xml 명으로 재정의. 그것을 템플릿으로 사용.
        ex) Spread_u18f.xml, Spread_u14a.xml -> Spread_u0001.xml, Spread_u0002.xml 변환.

        2. 변환된 스프레드는 활용하지 않고 단어를 넣을 때 자동추가되는 템플릿으로 활용.
        ex) Spread_u0001.xml 활용 -> Spread_u00011.xml,Spread_u00021.xml,Spread_u00031.xml ... 추가.


        :return: new_spread_templates : ["템플릿화 한 스프레드명","템플릿화 한 스프레드명"....] (단어 페이지가 담긴 스프레드 템플릿)

        """

        # Spreads 아래에 있는 베이스 템플릿.

        # 스프레드 템플릿의 페이지 번호
        number = 1

        self.spreads = os.listdir(self.spread_dir)

        # 스프레드 내용, 아이디 재정의.
        for spread in self.spreads:
            # 임시 파일 작성.
            with codecs.open(self.output + "tmp.tmp", 'w', encoding='utf-8') as tmp_file:
                with codecs.open(self.spread_dir + spread, 'r', encoding='utf-8') as spread_detail:
                    for line in spread_detail:
                        # 페이지 아이디 재정의를 위한 페이지번호 가져오기.
                        if "Page Self" in line:
                            # Name = 페이지번호.
                            matches = re.findall(r'Name=\"(.+?)\"', line)
                            number = int(int(matches[0]) / 2 + 1)
                            break;

                with codecs.open(self.spread_dir + spread, 'r', encoding='utf-8') as spread_detail:
                    story_rename = 1
                    temp_story_list = []
                    for line in spread_detail:
                        # 페이지 아이디 재정의.
                        if "PageCount" in line:
                            matches = re.findall(r'_(.+?)\.', spread)
                            line = str(line).replace(str(matches[0]), "u" + zfill_num(number))

                        # 해당 스프레드가 가진 스토리 정의.
                        if "ParentStory" in line:
                            matches = re.findall(r'ParentStory=\"(.+?)\"', line)
                            # print(matches)
                            renamed_story = "u" + zfill_num(number) + "_" + zfill_num(story_rename)
                            story_dic = {str(matches[0]): renamed_story}
                            temp_story_list.append(story_dic)
                            story_rename += 1
                            line = str(line).replace(str(matches[0]), renamed_story)
                        tmp_file.write(line)
                    self.story_lists_templates["u" + zfill_num(number)] = temp_story_list

            self.new_spread_templates.append(makefilename(1, number))
            shutil.move(self.output + 'tmp.tmp', self.spread_dir + makefilename(1, number) + ".xml")
            # 기존 스프레드 삭제
            os.remove(self.spread_dir + str(spread))
        # 스프레드 내용, 아이디 재정의. 끝.

        # 순서대로 출력을 위한 정렬.
        self.new_spread_templates.sort()

    # 스토리 초기화.
    def init_story(self):
        """
        스프레드명을 변경한 후 스토리명을 사용자가 알기쉽게 면경함.
        story_lists_templates: {"스프레드명":[{"이전스토리명":"템플릿화 한 스토리명"},{"이전스토리명":"템플릿화 한 스토리명"},....]
        list_new_story: {"스프레드명":["템플릿화 한 스토리명","템플릿화 한 스토리명"...]}

        """

        for key in self.story_lists_templates.keys():
            list = []
            # print(self.story_lists_templates[key])
            for item in self.story_lists_templates[key]:
                for i in item:
                    list.append(item[i])
                    with codecs.open(self.output + "tmp.tmp", 'w', encoding='utf-8') as tmp_file:
                        with codecs.open(self.story_dir + "Story_" + i + ".xml", 'r', encoding='utf-8') as story_detail:
                            for line in story_detail:
                                if "Self" in line:
                                    matches = re.findall(r'Self=\"(.+?)\"', line)
                                    line = str(line).replace(str(matches[0]), item[i])
                                tmp_file.write(line)
                            shutil.move(self.output + 'tmp.tmp', self.story_dir + "Story_" + item[i] + ".xml")
                            # 기존 스토리 삭제
                            # print(self.story_dir + "Story_" + i + ".xml")
                            os.remove(self.story_dir + "Story_" + i + ".xml")
        #
        # for key in self.story_lists_templates.keys():
        #     for item in self.story_lists_templates[key]:
        #         for i in item:
        #             try:
        #                 with codecs.open(self.output + "tmp.tmp", 'w', encoding='utf-8') as tmp_file:
        #                     with codecs.open(self.story_dir + "Story_" + i + ".xml", 'r',
        #                                      encoding='utf-8') as story_detail:
        #                         shutil.move(self.output + 'tmp.tmp', self.story_dir + "Story_" + item[i] + ".xml")
        #                         # 기존 스토리 삭제
        #                         # print(self.story_dir + "Story_" + i + ".xml")
        #                         os.remove(self.story_dir + "Story_" + i + ".xml")
        #             except:
        #                 pass

            # self.list_new_story[str(key)] = list

    # 스프레드, 스토리 넣기.
    def create_page(self):
        """
        초기화 한 스토리와 스프레드를 이용하여 책으로 생성.
        """
        self.firstcheck = True
        check_templates_info = "cover"
        new_spreads = []
        # spread_num = 1
        story_lists = {}
        # 책 커버부터 제작.
        loop_story_count = 1
        loop_spread_count = 1
        make_count = 0
        # # 총 챕터 수 만큼 반복(책터수만큼 반복페이지 만들기)
        # for index in range(self.total_count):
        voca_index = 0
        pron_index = 0
        mean_index = 0
        sentence_index = 0
        sentence_korean_index = 0
        star_index = 0
        pos_index = 0
        page_count = 0
        page_num = 1
        word_count = 1
        pa_index = 0
        pa_korean_index = 0

        def check_index(count):
            if voca_index == count:
                if voca_index == pron_index ==  pos_index:
                    return True
            else:
                return False
        while make_count < self.total_count:
            with codecs.open(self.output + "tmp.tmp", 'w', encoding='utf-8') as tmp_file:
                with codecs.open(self.spread_dir + self.new_spread_templates[
                    int(self.templates_info[check_templates_info])] + ".xml", 'r',
                                 encoding='utf-8') as spread_templates:
                    story_num = 1
                    temp_story_list = []
                    for line in spread_templates:
                        if "Spread Self" in line:
                            # 스프레드의 아이디 찾기
                            matches = re.findall(r'Spread Self=\"(.+?)\"', line)[0]  # 첫 스프레드면 1 두번째부턴 2

                            # 바꾼 스프레드 아이디
                            after_spread_name = "u" + zfill_num(
                                int(self.templates_info[check_templates_info]) + 1) + zfill_num(loop_spread_count)
                            loop_spread_count += 1
                            # 스프레드의 아이디 재정의.
                            line = str(line).replace(matches, after_spread_name)

                        if "ParentStory" in line:
                            # 내부 스토리 아이디 찾기.
                            matches = re.findall(r'ParentStory=\"(.+?)\"', line)[0]
                            self.list_new_story[matches]="0"
                            # 바꾼 스토리 아이디.
                            after_story_name = "u" + zfill_num(story_num) + zfill_num(
                                loop_story_count)

                            story_num += 1
                            loop_story_count += 1
                            # 스토리 아이디 재정의 .
                            line = str(line).replace(matches, after_story_name)
                            # 스토리 새로 작성.
                            with codecs.open(self.output + "tmp1.tmp", 'w', encoding='utf-8') as tmp_file1:
                                # 스프레드에 정의된 스토리 가져오기.
                                with codecs.open(self.story_dir + "Story_" + matches + ".xml", 'r',
                                                 encoding='utf-8') as s1:
                                    for s1_line in s1:
                                        # 스토리 설정.
                                        if "Story Self" in s1_line:
                                            match = re.findall(r'Story Self=\"(.+?)\"', s1_line)[0]

                                            # 스토리아이디 재정의.
                                            s1_line = str(s1_line).replace(match, after_story_name)

                                            # 내보낼 스토리아이디 형식설정.
                                            temp_story_list.append({match: str(after_story_name)})
                                            after_story_name = "Story_" + after_story_name + ".xml"
                                        if "#s1#" in s1_line:
                                            #★#
                                            if voca_index <= self.json_list[make_count][self.count]:
                                                star_count = int(self.json_list[make_count][
                                                    self.text_list][star_index]["importance"])
                                                replace_star = ""
                                                for i in range(star_count):
                                                    replace_star += "★"
                                                match = re.findall(r'' + "#s1#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, replace_star)
                                        if "#s2#" in s1_line:
                                            #★#
                                            if voca_index <= self.json_list[make_count][self.count]:
                                                star_count = int(self.json_list[make_count][
                                                    self.text_list][star_index]["importance"])
                                                replace_star = ""
                                                for i in range(3 - star_count):
                                                    replace_star += "★"
                                                match = re.findall(r'' + "#s2#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, replace_star)
                                                star_index += 1
                                        # 단어 집어넣기.
                                        if "#word#" in s1_line:
                                            if voca_index < self.json_list[make_count][self.count]:

                                                match = re.findall(r'' + "#word#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, self.json_list[make_count][
                                                    self.text_list][voca_index]["word"])
                                                # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                                voca_index += 1
                                            else:
                                                match = re.findall(r'' + "#word#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "ㅎ")

                                            # if "associate" == self.json_list[make_count][
                                            #         self.text_list][voca_index]["word"]:
                                            #     print(after_story_name)
                                            #     input()

                                        if "#pron#" in s1_line:
                                            if pron_index < self.json_list[make_count][self.count]:

                                                match = re.findall(r'' + "#pron#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, self.json_list[make_count][
                                                    self.text_list][pron_index]["pron"])
                                                # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                                pron_index += 1
                                            else:
                                                match = re.findall(r'' + "#pron#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "ㅎ")

                                        if "#pa#" in s1_line:
                                            strs = ""
                                            for key in self.json_list[make_count][
                                                self.text_list][pa_index]["derivative"]:
                                                strs += key + "\n"
                                                # print(key)
                                            match = re.findall(r'' + "#pa#" + '', s1_line)[0]
                                            s1_line = str(s1_line).replace(match, strs)
                                            # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                            pa_index += 1

                                        if "#pa_k#" in s1_line:
                                            strs = ""
                                            for _, key in self.json_list[make_count][
                                                self.text_list][pa_korean_index]["derivative"].items():
                                                strs += key + "\n"
                                                # print(key)
                                            match = re.findall(r'' + "#pa_k#" + '', s1_line)[0]
                                            s1_line = str(s1_line).replace(match, strs)
                                            # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                            pa_korean_index += 1

                                        if "#p#" in s1_line:
                                            try:
                                                match = re.findall(r'' + "#p#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, str(page_num))
                                                # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                                page_num += 1
                                                page_count += 1
                                            except:
                                                match = re.findall(r'' + "#p#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "")

                                        # if ("#in" in s1_line) and self.firstcheck:
                                        #     print(self.json_list[make_count][
                                        #         self.text_list][mean_index])
                                        #     story_str = self.json_list[make_count][
                                        #         self.text_list][mean_index]["meaning"]
                                        #     match = re.findall(r'#in' + "" + '', s1_line)[0]
                                        #     s1_line = str(s1_line).replace(match, story_str)
                                        if "#meaning#" in s1_line:
                                            if mean_index < self.json_list[make_count][self.count]:
                                                story_str=""
                                                if check_templates_info == "test":
                                                    story_str = self.json_list[make_count][
                                                        self.text_list][mean_index]["meaning"] + " / " + self.json_list[make_count][
                                                        self.text_list][mean_index]["meaning2"]

                                                    mean_index += 1
                                                    if mean_index == self.json_list[make_count][self.count]:
                                                        mean_index = 0

                                                elif (check_templates_info == "main" or check_templates_info == "main2"):
                                                    if self.checkmean == 0:
                                                        story_str = self.json_list[make_count][
                                                            self.text_list][mean_index]["meaning"]
                                                        self.checkmean = 1
                                                    elif self.checkmean == 1:
                                                        story_str = self.json_list[make_count][
                                                            self.text_list][mean_index]["meaning2"]
                                                        mean_index += 1
                                                        self.checkmean = 0

                                                match = re.findall(r'#meaning#' + "" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, story_str)
                                                # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                                # mean_index += 1
                                            else:
                                                match = re.findall(r'#meaning#' + "" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "ㅋ")
                                        # input()
                                        if "#pos#" in s1_line:
                                            if pos_index < self.json_list[make_count][self.count]:

                                                match = re.findall(r'' + "#pos#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, self.json_list[make_count][
                                                    self.text_list][pos_index]["pos"][0])
                                                # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                                pos_index += 1
                                            else:
                                                match = re.findall(r'' + "#pos#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "")
                                        if "#count#" in s1_line:
                                            try:
                                                match = re.findall(r'' + "#count#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, str(str(word_count).zfill(4)))
                                            except:
                                                match = re.findall(r'' + "#count#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "")
                                            word_count += 1

                                        if "#sentence#" in s1_line:
                                            if sentence_index < self.json_list[make_count][self.count]:

                                                match = re.findall(r'' + "#sentence#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, self.json_list[make_count][
                                                    self.text_list][sentence_index]["sentence"])
                                                # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                                sentence_index += 1
                                            else:
                                                match = re.findall(r'' + "#sentence#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "")

                                        if "#sentence_korean#" in s1_line:

                                            if sentence_korean_index < self.json_list[make_count][self.count]:

                                                match = re.findall(r'' + "#sentence_korean#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, self.json_list[make_count][
                                                    self.text_list][sentence_korean_index]["sentence_korean"])
                                                # story 순서가 뒤죽박죽이기때문에 각각의 인덱스를 설정.
                                                sentence_korean_index += 1
                                            else:
                                                match = re.findall(r'' + "#sentence_korean#" + '', s1_line)[0]
                                                s1_line = str(s1_line).replace(match, "")
                                        if "#n" in s1_line:
                                            match = re.findall(r'#n', s1_line)[0]
                                            s1_line = str(s1_line).replace(match, str(self.json_list[make_count]["chapter"]))

                                        tmp_file1.write(s1_line)
                                    story_lists[after_spread_name] = temp_story_list

                                    shutil.move(self.output + 'tmp1.tmp', self.story_dir + after_story_name)
                        tmp_file.write(line)
                        """
                        templates_info = {"cover": 0, "chap": 1 ,"main": 2, "main2": 3, "test": 1}
                        """
            print(page_num)
            if check_templates_info == "cover":
                check_templates_info = "chap"
            elif check_templates_info == "chap":
                check_templates_info = "test"
            elif check_templates_info == "main":
                # if voca_index == 4 and mean_index == 4:
                if check_index(4):
                    check_templates_info = "main2"
                    page_count = 0
            elif check_templates_info == "test":
                mean_index = 0
                check_templates_info = "main"
                try:
                    if self.json_list[make_count][self.count] < (self.word_per_page / 2):
                        check_templates_info = "main2"
                except:
                    # TODO 끝인 경우
                    pass
            # elif check_templates_info == "main2":
            #     make_count += 1
            #     voca_index = 0
            #     mean_index = 0
            #     check_templates_info = "test"
            elif check_templates_info == "main2":

                if check_index(25):
                    make_count += 1
                    voca_index = 0
                    pron_index = 0
                    mean_index = 0
                    sentence_index = 0
                    sentence_korean_index = 0
                    pos_index = 0
                    check_templates_info = "chap"
                    page_count = 0
                    star_index = 0
                    pa_index = 0
                    pa_korean_index = 0
            # else:
            #     if (self.json_list[make_count][self.count] - voca_index) > 0:
            #         if (self.json_list[make_count][self.count] - voca_index) <= (self.word_per_page / 2):
            #             check_templates_info = "main2"
            #         else:
            #             check_templates_info = "main"
            #     if (mean_index == self.json_list[make_count][self.count]) and (voca_index == self.json_list[make_count][self.count]):
            #         check_templates_info = "test"
            #         voca_index = 0
            #         mean_index = 0

            new_spreads.append("Spread_" + after_spread_name)
            shutil.move(self.output + 'tmp.tmp', self.spread_dir + "Spread_" + after_spread_name + ".xml")
            # 챕터 페이지 형성.
        self.create_designmap(new_spreads, story_lists)

    # 디자인맵 초기화.
    def create_designmap(self, new_spread_templates, story_lists_templates):
        """

        :param new_spread_templates:
        :param story_lists_templates:
        :return: None

        만들어놓은 것들을 디자인맵에 등록을 해야 페이지가 추가가 되기 때문에
        여기서 추가해준다.

        """

        # designmap 에 바꾼 아이디 재정의.
        check_spread = True
        check_story = True
        stories = ""
        for key in story_lists_templates.keys():
            for item in story_lists_templates[key]:
                for i in item:
                    stories += item[i] + " "
        with codecs.open(self.output + "tmp.tmp", 'w', encoding='utf-8') as tmp_file:
            with codecs.open(self.tmp_dir + "designmap.xml", 'r', encoding='utf-8') as s:
                for line in s:
                    if "StoryList" in line:
                        matches = re.findall(r'StoryList=\"(.+?)\"', line)

                        line = str(line).replace(str(matches[0]), stories)
                    if ":Spread src" in line:
                        if check_spread:
                            check_spread = False
                            pass
                        else:
                            continue
                        for new_spread in new_spread_templates:
                            line = "<idPkg:Spread src=" + '"Spreads/' + new_spread + '.xml" />\n'
                            tmp_file.write(line)
                        continue

                    if ":Story src" in line:
                        if check_story:
                            check_story = False
                            pass
                        else:
                            continue
                        for new_story in stories.split(" "):
                            if new_story == "":
                                break;
                            line = "<idPkg:Story src=" + '"Stories/' + "Story_" + new_story + '.xml" />\n'
                            tmp_file.write(line)
                        continue

                    tmp_file.write(line)
            shutil.move(self.output + 'tmp.tmp', self.tmp_dir + "designmap.xml")
#
# j2i2p = CreatePdf()
# j2i2p.create()