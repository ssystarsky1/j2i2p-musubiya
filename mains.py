import os
import sys

from PyQt5 import QtCore
from PyQt5.QtWidgets import *

import j2i2p


class Main(QWidget):
    def __init__(self):
        try:
            self.jsons = os.listdir(os.path.dirname(sys.argv[0]) +'/jsons')
        except:
            try:
                self.jsons = os.listdir('./jsons')
            except:
                self.jsons = os.listdir(os.listdir(os.path.abspath('jsons')))
        global progress
        progress = ""
        super().__init__()
        self.initUI()
        self.threadclass = ThreadClass()

    def initUI(self):
        btn1 = QPushButton('scan json')
        btn1.clicked.connect(self.scan_btn)

        btn2 = QPushButton('stark task')
        btn2.clicked.connect(self.start_btn)

        vbox = QVBoxLayout()
        vbox.addWidget(btn1)
        vbox.addWidget(btn2)

        groupbox1 = QGroupBox('json file')
        self.bx1 = QVBoxLayout()
        self.bx1Label = QTextBrowser()
        self.bx1.addWidget(self.bx1Label)
        groupbox1.setLayout(self.bx1)

        scrollArea = QScrollArea()
        groupbox2 = QGroupBox('progress state')
        global bx2Label
        bx2Label = QLabel("0%")
        self.bx2 = QVBoxLayout()
        global bx2Text
        space = "\t\t\t\t\t\t\n"
        temp = ""
        for _ in range(len(self.jsons)):
            for k in range(7):
                temp += space
        bx2Text = QLabel(temp)
        scrollArea.setWidget(bx2Text)
        bx2Text.setContentsMargins(1, 1, 1, 1)
        self.bx2.addWidget(bx2Label)
        self.bx2.addWidget(scrollArea)
        groupbox2.setLayout(self.bx2)

        scrollArea1 = QScrollArea()
        groupbox3 = QGroupBox('completion state')
        self.bx3 = QVBoxLayout()
        global bx3Text
        space = "\t\t\n"
        temp = ""
        for _ in range(len(self.jsons)):
            temp += space
        bx3Text = QLabel(temp)
        scrollArea1.setWidget(bx3Text)
        bx3Text.setContentsMargins(1, 1, 1, 1)
        self.bx3.addWidget(scrollArea1)
        groupbox3.setLayout(self.bx3)

        hbox = QHBoxLayout()
        hbox.addWidget(groupbox1)
        hbox.addWidget(groupbox2)
        hbox.addWidget(groupbox3)
        hbox.addLayout(vbox)

        self.setWindowTitle('trans json to pdf')
        self.setLayout(hbox)
        self.setGeometry(100, 100, 900, 300)
        self.show()

    def scan_btn(self):
        texts = ""
        for i in self.jsons:
            texts += i + "\n"
        self.bx1Label.setText(texts)

    def start_btn(self):
        # self.craft()
        self.threadclass.start()
        # my_thread = threading.Thread(target=self.craft())
        # my_thread.start()


class ThreadClass(QtCore.QThread):
    def __init__(self, parent=None):
        super(ThreadClass, self).__init__(parent)

    def run(self):
        prg = ""
        cmp = ""
        self.check = True
        try:
            self.jsons = os.listdir(os.path.dirname(sys.argv[0]) +'/jsons')
        except:
            try:
                self.jsons = os.listdir('./jsons')
            except:
                self.jsons = os.listdir(os.path.dirname(__file__) + "/jsons")
            self.check = False
        progress_per_each = 100 / len(self.jsons)
        progress_count = 0

        for i in self.jsons:
            prg += i + " task started.\n"
            bx2Text.setText(prg)
            obj = j2i2p.CreatePdf()
            obj.set_user_name(i)
            if self.check:
                obj.set_file_dir(os.path.dirname(sys.argv[0]) + "/")
                path = os.path.dirname(sys.argv[0]) + '/jsons/' + i
            else:
                path = './jsons/' + i
                obj.set_file_dir("")
            obj.set_user_json(path)
            prg += "json setting complete.\n"
            bx2Text.setText(prg)
            obj.unpack_idml()
            prg += "idml unpacked.\n"
            bx2Text.setText(prg)
            obj.init_spread()
            obj.init_story()
            obj.create_page()
            prg += "page setting completed.\n"
            bx2Text.setText(prg)
            obj.create_idml()
            prg += "idml created.\nnow saving: " + i + ".pdf....\n"
            bx2Text.setText(prg)
            # obj.export( "test1.json", "./jsons/")  # pdf 만들기.
            prg += i + ".pdf created.\n"
            bx2Text.setText(prg)
            cmp += i + "\n"
            bx3Text.setText(cmp)
            progress_count += progress_per_each
            bx2Label.setText(str(progress_count) + "%")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Main()
    sys.exit(app.exec_())
